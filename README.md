![カレンダー例](https://gitlab.com/teten927/vue.js-calendar/-/raw/master/example.png)

Vue.js用のカレンダー作成用ライブラリ。
下記機能搭載。
- 高さ、幅、色など各デザインカスタマイズ可能。
- 各日付のマスに、ユーザが作成したテンプレート構文を挿入可能。

## Installation
```
npm install calendar-framewoek
# or
yarn add calendar-framework
```

## Usage

### Example

```Vue
<template>
  <div>
    <div>
      <button @click="() => $refs.calendar.prev()">◀ Prev</button>
      <button @click="() => $refs.calendar.next()">Next ▶</button>
    </div>
    <Calendar
      ref="calendar"
      outsideDateColor="rgb(70,70,70)"
      itemMinWidth="290px"
      itemMaxWidth="290px"
      itemMinHeight="200px"
      itemMaxHeight="200px"
      weekFontSize="40px"
      weekdayColor="white"
      dateColor="white"
      dateFontSize="24px"
      borderStyle="double"
      borderWidth="6px"
      borderColor="white"
      todayBorderColor="yellow"
      backgroundColor="black"
    >
      <template
        v-for="element in [4, 5, 11, 12, 18, 19, 20, 24, 25, 26]"
        v-slot:[element]
      >
        <div :key="element">
          <p style="color: white; color: red">休暇</p>
        </div>
      </template>
      <template v-slot:default style="color: white">
        <p style="color: white">やることリスト</p>
        <input type="text" />
        <input type="text" />
        <input type="text" />
        <button>決定</button>
      </template>
    </Calendar>
  </div>
</template>

<script>
import Calendar from "calendar-framework";

export default {
  components: {
    Calendar,
  },
};
</script>
```
### v-model
現在の日付のDate型の値を返す。
```Vue
<Calendar v-model="currentDate">
```

### Props
|  Name  |  Type  |  Default  |  Detail  |
| ---- | ---- | ---- | ---- |
|  year  |  Number  |  現在日時の年。  |  取得したいカレンダーの年。  |
|  month  |  Number  |  現在日時の月。  |  取得したいカレンダーの月。  |
|  weekHeight  |  String  |  "60px"  |  カレンダーの曜日部分の各マスの高さ。  |
|  itemMinHeight  |  String  |  "0"  |  カレンダーの日付部分の各マスの最小の高さ。<br>※高さを固定にしたい場合、「itemMaxHeight」にも同じ値を入れる。  |
|  itemMaxHeight  |  String  |  "auto"  |  カレンダーの日付部分の各マスの最大の高さ。<br>※高さを固定にしたい場合、「itemMinHeight」にも同じ値を入れる。  |
|  itemMinWidth  |  String  |  "0"  |  カレンダーの日付部分の各マスの最小の幅。<br>※幅を固定にしたい場合、「itemMaxWidth」にも同じ値を入れる。  |
|  itemMaxWidth  |  String  |  "auto"  |  カレンダーの日付部分の各マスの最大の幅。<br>※幅を固定にしたい場合、「itemMinWidth」にも同じ値を入れる。  |
|  outsideDateColor  |  String  |  "gray"  |  カレンダーの最初と最後の、該当月外の日付部分の色。  |
|  weekNames  |  Array  |  [<br> "SUN",<br> "MON",<br> "TUE",<br> "WED",<br> "THU",<br> "FRY",<br> "SAT"<br>]  |  カレンダーの曜日部分の表示内容。日曜日から順に配列に入れる。<br>null値を入れた箇所には、デフォルトの表示で置き換えられる。  |
|  saturdayColor  |  String  |  "blue"  |  カレンダーの曜日の「土曜日」に該当する文字のフォント色。  |
|  sundayColor  |  String  |  "red"  |  カレンダーの曜日の「日曜日」に該当する文字のフォント色。  |
|  weekdayColor  |  String  |  "black"  |  カレンダーの曜日の「土曜日」、「日曜日」以外に該当する文字のフォント色。  |
|  weekFontSize  |  String  |  "26px"  |  カレンダーの曜日の文字のフォントサイズ。  |
|  dateColor  |  String  |  "black"  |  カレンダーの日付の文字のフォント色。  |
|  dateFontSize  |  String  |  "16px"  |  カレンダーの日付の文字のフォントサイズ。  |
|  borderColor  |  String  |  "black"  |  カレンダーの線の色。  |
|  borderStyle  |  String  |  "double"  |  カレンダーの線のスタイル。  |
|  borderWidth  |  String  |  "4px"  |  カレンダーの線の幅。  |
|  todayBorderColor  |  String  |  -  |  現在の日付が表示月の中にあれば、指定した色で囲む。<br>本プロパティを未設定の場合、現在の日付があっても、囲まれない。  |
|  backgroundColor  |  String  |  "transparent"  |  カレンダー全体の背景色。  |

### Template insertion
```Vue
<Calendar>
    <template v-slot: ##設定したい日付## >
        ##設定するテンプレート##
    </template>
</Calendar>
```

複数設定する場合
```Vue
<Calendar>
    <template
        v-for="element in [##設定する日付の配列##]"
        v-slot:[element]
        >
        <div :key="element">
            ##設定するテンプレート##
        </div>
    </template>
</Calendar>
```

デフォルトを設定する場合
```Vue
<Calendar>
    <template v-slot:default>
        ##設定するテンプレート##
    </template>
</Calendar>
```

### To next month or previous month
```Vue
<button @click=() => $refs.calendar.prev()>◀ prev</button>
<button @click=() => $refs.calendar.next()>next ▶</button>
<Calendar ref="calendar">
```